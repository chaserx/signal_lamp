# signal_lamp

Working from home is great, but getting into a routine with the rest of the family at home can be a challenge. 

I repurposed an old [Blink1](https://blink1.thingm.com/) as a signal flag for my availability when someone enters the office as my back is to the door. Using some velcro, I attached it to a short USB extender onto the bback of my monitor. 

## Requirements

- install the blink-tool application. 
-   mac: `brew install blink1`

## Usage

You can then use commands like `blink1-tool --green -b 100` to set the color of the LEDs as you like. You can also specify RGB values and other options. Run `blink1-tool -h` for detailed options and examples. There are other libraries to connect to the blink1 including Ruby gems. 

Included here is an Alfred workflow to change quickly and easily set the colors and brightness based on busy or free arguments to the workflow keyword, `sg`. 
